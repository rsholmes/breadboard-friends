breadboard-friends
==================

A collection of cute breakout boards riding solderless breadboards, mostly for analog audio works

Forked from pichenettes' version and identical except:
- Eagle brd and sch files converted to XML (making possible import into KiCad)
- KiCad versions of boards and schematics
- Added Thonkiconn version of eurojacks
- Added remix of pots
- Added pads board

The PCB layouts and schematics by Émelie Gillet and their derivatives are released under a Creative Commons cc-by-sa 3.0 license. Those by Rich Holmes are released under a Creative Commons CC0 1.0 license.
